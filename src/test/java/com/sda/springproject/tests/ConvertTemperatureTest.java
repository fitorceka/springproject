package com.sda.springproject.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

public class ConvertTemperatureTest {

    @ParameterizedTest
    @EnumSource(value = ConvertTemperature.class)
    public void checkIfTempIsLowerThan1000(ConvertTemperature conv) {
        Assertions.assertTrue(conv.convertTemp(10) < 1000);
    }

    @ParameterizedTest
    @EnumSource(value = ConvertTemperature.class,
            names = {"CELSIUS_TO_KELVIN", "CELSIUS_TO_FAHRENHEIT", "KELVIN_TO_CELSIUS"},
            mode = EnumSource.Mode.EXCLUDE)
    public void checkWaterModeBasedOnFahrenheitToCelsius(ConvertTemperature conv) {
        double readTemp = 22.5;
        double celsius = conv.convertTemp(readTemp);

        Assertions.assertEquals("Solid", Check.checkWaterMode(celsius));
    }
}
