package com.sda.springproject.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class CheckTest {

    @ParameterizedTest
    @ValueSource(ints = {-5, -1, 0})
    public void testInvalidGradeBelowRange(int num) {
        Assertions.assertFalse(Check.isValidGrade(num), "The grade is below 1");
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 10})
    public void testValidGradeRange(int num) {
        Assertions.assertTrue(Check.isValidGrade(num), "The grade is inside range");
    }

    @ParameterizedTest
    @ValueSource(ints = {11, 10000})
    public void testInvalidGradeOverRange(int num) {
        Assertions.assertFalse(Check.isValidGrade(num), "The grade is over range");
    }

    @ParameterizedTest
    @ValueSource(strings = {"hgf", "d"})
    public void testInvalidStrings(String str) {
        Assertions.assertFalse(Check.isBiggerThan3(str));
    }

    @ParameterizedTest
    @ValueSource(strings = {"hgfdfhd", "dsdgsdgsdgsdgfsdgsedg"})
    public void testValidStrings(String str) {
        Assertions.assertTrue(Check.isBiggerThan3(str));
    }

    @ParameterizedTest
    @CsvSource({" JaVA  ,java", "SDa ,sda", "TEMP,temp"})
    public void checkLower(String in, String expected) {
        Assertions.assertEquals(expected, Check.convertLowerCase(in));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/input.csv", numLinesToSkip = 1, delimiter = ';')
    public void checkLowerFromCsv(String in, String expected) {
        Assertions.assertEquals(expected, Check.convertLowerCase(in));
    }

    @ParameterizedTest
    @MethodSource("getStreamOf")
    public void checkLowerFromMethod(String in) {
        Assertions.assertEquals("java", Check.convertLowerCase(in));
    }

    @ParameterizedTest
    @MethodSource("getAlsoExpected")
    public void checkLowerFromMethodArguments(String in, String expected) {
        Assertions.assertEquals(expected, Check.convertLowerCase(in));
    }

    @ParameterizedTest
    @ArgumentsSource(StringArguments.class)
    public void checkLowerFromArguments(String in, String expected) {
        Assertions.assertEquals(expected, Check.convertLowerCase(in));
    }

    @Test
    public void checkException() {
        IllegalArgumentException exp = Assertions.assertThrows(IllegalArgumentException.class,
                () -> Check.divide(10.0, 1), "Exp not was thrown");

        Assertions.assertEquals("Cannot pass 0 param for divider!", exp.getMessage());
    }

    @Test
    void shouldThrowIllegalArgumentException() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> Check.divide(10, 0))
                .withMessage("Cannot pass 0 param for divider!")
                .withNoCause();
    }

    static Stream<String> getStreamOf() {
        return Stream.of("JaVA", "SDa", "TEMP");
    }

    static Stream<Arguments> getAlsoExpected() {
        return Stream.of(Arguments.of("JaVA", "java"),
                Arguments.of("SDa ", "sda"),
                Arguments.of(" TEMP ", "temp"));
    }
}
