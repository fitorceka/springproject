package com.sda.springproject.service;

import com.sda.springproject.entity.StudentEntity;
import com.sda.springproject.exceptions.NotFoundException;
import com.sda.springproject.repository.StudentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class StudentServiceTest {

    @Mock
    private StudentRepository repository;

    @InjectMocks
    private StudentService service;

    @Test
    public void testFindById() {
        //given
        StudentEntity std = new StudentEntity();
        std.setId(1L);
        std.setLastName("Fisteku");
        std.setFirstName("Filan");
        Mockito.when(repository.findById(ArgumentMatchers.any())).thenReturn(Optional.of(std));

        //when
        Optional<StudentEntity> returned = service.findById(ArgumentMatchers.any());

        //then
        assertThat(returned).isNotNull();
        assertThat(returned.get().getId()).isEqualTo(1L);
        assertThat(returned.get().getFirstName()).isEqualTo("Filan");
    }

    @Test
    public void testFindByIdThrows() {
        //given

        Mockito.when(service.findById(ArgumentMatchers.any())).thenThrow(new NotFoundException("Student is not found"));

        //then
        assertThatExceptionOfType(NotFoundException.class).isThrownBy(() -> service.findById(5L));
    }

    @Test
    public void testVerifyFindById() {
        //given
        StudentEntity std = new StudentEntity();
        std.setId(1L);
        std.setLastName("Fisteku");
        std.setFirstName("Filan");
        Mockito.when(repository.findById(ArgumentMatchers.any())).thenReturn(Optional.of(std));

        //when
        Optional<StudentEntity> returned = service.findById(ArgumentMatchers.any());

        //then
        Mockito.verify(repository, Mockito.times(1)).findById(ArgumentMatchers.any());
    }

    @Test
    public void testVerifyFindAll() {
        //given
        StudentEntity std1 = new StudentEntity();
        std1.setId(1L);
        std1.setLastName("Fisteku");
        std1.setFirstName("Filan");
        StudentEntity std2 = new StudentEntity();
        std2.setId(2L);
        std2.setLastName("Fisteku");
        std2.setFirstName("Filan");
        Mockito.when(service.findById(1L)).thenReturn(Optional.of(std1));
        Mockito.when(service.findById(2L)).thenReturn(Optional.of(std2));
//        Mockito.when(service.findAll(List.of(1L, 2L))).thenReturn(List.of(std1, std2));

        // When
        List<StudentEntity> result = service.findAll(List.of(1L, 2L));

        // Then
        // Verify that findById is called twice with the expected parameters
        Mockito.verify(repository, Mockito.times(1)).findById(1L);
        Mockito.verify(repository, Mockito.times(1)).findById(2L);

        // Assert the result to ensure the service method works as expected
        assertEquals(2, result.size());
        assertTrue(result.contains(std1));
        assertTrue(result.contains(std2));
    }
}
