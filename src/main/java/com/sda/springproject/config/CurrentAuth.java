package com.sda.springproject.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class CurrentAuth {

    public Authentication getCurrent() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
}
