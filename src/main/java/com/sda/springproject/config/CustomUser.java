package com.sda.springproject.config;

import com.sda.springproject.entity.StudentEntity;
import com.sda.springproject.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUser implements UserDetailsService {

    @Autowired
    private StudentService service;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return service.findByEmail(username)
                .map(this::toUserDetails)
                .orElseThrow(() -> new UsernameNotFoundException("Bad credentials"));

    }

    private UserDetails toUserDetails(StudentEntity student) {
        return User.builder()
                .username(student.getEmail())
                .password(student.getPassword())
                .authorities(new SimpleGrantedAuthority(student.getRole().toString()))
                .build();
    }
}
