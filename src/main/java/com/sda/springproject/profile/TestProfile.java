package com.sda.springproject.profile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

// this bean will be created only if profile is not dev
@Component
@Profile("!dev")
public class TestProfile {

    public TestProfile() {
        System.out.println("This is a profile bean");
    }
}
