package com.sda.springproject.mapper;

import com.sda.springproject.entity.StudentEntity;
import com.sda.springproject.model.StudentModel;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class StudentMapper {

    private final PasswordEncoder passwordEncoder;

    public StudentModel toModel(StudentEntity entity) {
        if (entity == null) {
            return null;
        }

        return new StudentModel(entity.getId(), entity.getFirstName(),
                entity.getLastName(), entity.getBirthday(),
                entity.getEmail(), entity.getPassword(), entity.getRole());
    }

    public StudentEntity toEntity(StudentModel model) {
        if (model == null) {
            return null;
        }
        return new StudentEntity(model.getFirstName(),
                model.getLastName(), model.getBirthday(), model.getEmail(),
                passwordEncoder.encode(model.getPassword()), model.getRole());
    }
}
