package com.sda.springproject.controller;

import com.sda.springproject.model.StudentModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class TestController {

//    @RequestMapping(path = "/hello", method = RequestMethod.GET)
    @GetMapping("/hello")
    @ResponseBody
    public String getHelloWorld() {
        return "Hello world";
    }

    @RequestMapping(path = "/hello/{id}", method = RequestMethod.GET)
    // @GetMapping("/hello/{id}")
    @ResponseBody
    public String getHelloWorldId(@PathVariable("id") int id) {
        return "Hello world " + id;
    }

    @RequestMapping(path = "/student", method = RequestMethod.POST)
    //@PostMapping("/student") the same as the annotation above
    public ResponseEntity<StudentModel> saveStudent(@RequestBody StudentModel model) {
        System.out.println(model.toString());
        return ResponseEntity.ok().body(model);
    }

    @RequestMapping(path = "/student", method = RequestMethod.PUT)
    public ResponseEntity<StudentModel> updateStudent(@RequestBody StudentModel model) {
        List<StudentModel> models = students();

        Optional<StudentModel> foundStudent = models.stream().filter(s -> s.getId() == model.getId()).findFirst();

        if (foundStudent.isPresent()) {
            updateStudent(foundStudent.get(), model);
        }
        else {
            throw new RuntimeException("Cannot find student with id: " + model.getId());
        }

        System.out.println("Updated student: " + foundStudent.get().toString());

        return ResponseEntity.ok().body(foundStudent.get());
    }

    //@RequestMapping(path = "/student/delete/{id}", method = RequestMethod.DELETE) used for path variabke
    @RequestMapping(path = "/student/delete", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteStudent(@RequestParam("studentId") int id) {
        List<StudentModel> newList = students();

        Optional<StudentModel> foundStudent = newList.stream().filter(s -> s.getId() == id).findFirst();

        foundStudent.ifPresent(newList::remove);

        System.out.println(newList.size());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    private List<StudentModel> students() {
//        StudentModel s1 = new StudentModel(1L, "Filan1", "Fistek1", LocalDate.of(1999, 10, 10));
//        StudentModel s2 = new StudentModel(2L, "Filan2", "Fistek2", LocalDate.of(2002, 10, 10));
//        StudentModel s3 = new StudentModel(3L, "Filan3", "Fistek3", LocalDate.of(2003, 10, 10));

        //return new ArrayList<>(List.of(s1, s2, s3));
        return new ArrayList<>();
    }

    private StudentModel updateStudent(StudentModel old, StudentModel newS) {
        old.setFirstName(newS.getFirstName());
        old.setLastName(newS.getLastName());
        old.setBirthday(newS.getBirthday());

        return old;
    }
}
