//package com.sda.springproject.controller;
//
//import com.sda.springproject.config.AuthRequest;
//import com.sda.springproject.service.AuthService;
//import lombok.AllArgsConstructor;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@AllArgsConstructor
//public class Login {
//
//    private AuthService authService;
//
//    @PostMapping("/login")
//    public ResponseEntity<Void> login(@RequestBody AuthRequest request) {
//        SecurityContextHolder.clearContext();
//        authService.authenticate(request);
//        return ResponseEntity.ok().build();
//    }
//}
