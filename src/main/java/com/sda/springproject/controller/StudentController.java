package com.sda.springproject.controller;

import com.sda.springproject.config.CurrentAuth;
import com.sda.springproject.exceptions.NotFoundException;
import com.sda.springproject.mapper.StudentMapper;
import com.sda.springproject.model.StudentModel;
import com.sda.springproject.service.StudentService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;
    private final StudentMapper mapper;
    private final CurrentAuth currentAuth;


    @GetMapping("/student/{studentId}")
    public ResponseEntity<StudentModel> getById(@PathVariable("studentId") Long id) {
        return ResponseEntity.ok(mapper.toModel(studentService
                .findById(id).orElseThrow(() -> new NotFoundException("Cannot find student by id!"))));
    }

    @PostMapping("/student")
//    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<StudentModel> saveStudent(@RequestBody @Valid StudentModel model) {
         return ResponseEntity.ok(mapper.toModel(studentService.save(mapper.toEntity(model))));
    }

    @PutMapping("/student")
    public ResponseEntity<StudentModel> updateStudent(@RequestBody StudentModel model) {
        return ResponseEntity.ok(mapper.toModel(studentService.updateStudent(model)));
    }

    @DeleteMapping("/student")
    public ResponseEntity<Void> updateStudent(@RequestParam("studentId") Long id) {
        studentService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/student")
    public ResponseEntity<List<StudentModel>> getAll() {
        System.out.println(currentAuth.getCurrent());
        return ResponseEntity.ok(studentService.getAll().stream().map(mapper::toModel).toList());
    }

    @GetMapping("/student/fn")
    public ResponseEntity<List<StudentModel>> getAll(@RequestParam("firstName") String fn) {
        return ResponseEntity.ok(studentService.getAllByFirstName(fn).stream().map(mapper::toModel).toList());
    }

    @GetMapping("/student/date")
    public ResponseEntity<List<StudentModel>> getAll(@RequestParam("from") LocalDate from, @RequestParam("to") LocalDate to) {
        return ResponseEntity.ok(studentService.getAllByBirthdayBetween(from, to).stream().map(mapper::toModel).toList());
    }
}
