package com.sda.springproject.repository;

import com.sda.springproject.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Long> {

    //this query is provided direclty by method name
//    @Query("select S from StudentEntity S where S.firstName = :firstName")
    List<StudentEntity> findAllByFirstName(String firstName);

    // e njeta si findById()
//    @Query("select S from StudentEntity S where S.id = :studentId")
//    Optional<StudentEntity> getByIdCustom(@Param("studentId") Long id);

    // this is an sql query
    @Query(value = "Select * from student where student.first_name = :firstName", nativeQuery = true)
    List<StudentEntity> getAllByFirstName(String firstName);

    @Query("select S from StudentEntity S where S.birthday >= :from AND S.birthday <= :to")
    List<StudentEntity> getByBirthdayBetween(LocalDate from, LocalDate to);

    Optional<StudentEntity> findByEmail(String email);
}
