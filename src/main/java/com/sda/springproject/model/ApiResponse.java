package com.sda.springproject.model;

import lombok.Data;

@Data
public class ApiResponse {

    private String message;
}
