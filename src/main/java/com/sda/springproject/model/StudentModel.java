package com.sda.springproject.model;

import com.sda.springproject.enums.Role;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@ConfigurationProperties("message.properties")
public class StudentModel {

    private Long id;

    @NotNull
    @Size(min = 3, max = 32, message = "sizefirstname")
    private String firstName;

    @NotNull
    @Size(min = 3, max = 32)
    private String lastName;

    @NotNull
    private LocalDate birthday;

    private String email;

    private String password;
    private Role role;
}
