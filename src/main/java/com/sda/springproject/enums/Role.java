package com.sda.springproject.enums;

public enum Role {

    ADMIN, STUDENT
}
