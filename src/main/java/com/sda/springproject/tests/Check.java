package com.sda.springproject.tests;

public class Check {

    public static boolean isValidGrade(int grade) {
        return grade >= 1 && grade <= 10;
    }

    public static boolean isBiggerThan3(String str) {
        return str.length() > 3;
    }

    public static String checkWaterMode(double temp) {
        if (temp < 0) {
            return "Solid";
        }
        else if (temp >= 0 && temp <= 100) {
            return "Liquid";
        }
        else {
            return "Gas";
        }
    }

    public static String convertLowerCase(String input) {
        return input.trim().toLowerCase();
    }

    public static double divide(double a, double b) {
        if (b == 0) {
            throw new IllegalArgumentException("Cannot pass 0 param for divider!");
        }

        return a/b;
    }
}
