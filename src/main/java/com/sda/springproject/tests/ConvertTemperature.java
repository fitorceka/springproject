package com.sda.springproject.tests;

import java.util.function.Function;

public enum ConvertTemperature {

    CELSIUS_TO_KELVIN(c -> c + 273.15),
    KELVIN_TO_CELSIUS(k -> k - 273.15),
    CELSIUS_TO_FAHRENHEIT(c -> c * 9 / 5 + 32),
    FAHRENHEIT_TO_CELSIUS(f -> (f - 32) / 1.8);

    private Function<Double, Double> converter;

    ConvertTemperature(Function<Double, Double> converter) {
        this.converter = converter;
    }

    public double convertTemp(double temp) {
        return converter.apply(temp);
    }
}
