package com.sda.springproject;

import com.sda.springproject.config.CurrentAuth;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.Authentication;

@SpringBootApplication(scanBasePackages = {"com.sda.springproject", "thymeleaf"})
//@ComponentScan("thymeleaf")
public class SpringProjectApplication {

	public static void main(String[] args) {
		var context = SpringApplication.run(SpringProjectApplication.class, args);
		CurrentAuth auth = context.getBean(CurrentAuth.class);
		Authentication a = auth.getCurrent();
		System.out.println(a);
	}

}
