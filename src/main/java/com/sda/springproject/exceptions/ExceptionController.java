package com.sda.springproject.exceptions;

import com.sda.springproject.model.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionController {

    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<ApiResponse> handleError(Exception e) {
        ApiResponse response = new ApiResponse();
        response.setMessage(e.getLocalizedMessage());

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
}
