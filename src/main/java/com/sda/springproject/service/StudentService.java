package com.sda.springproject.service;

import com.sda.springproject.entity.StudentEntity;
import com.sda.springproject.exceptions.NotFoundException;
import com.sda.springproject.model.StudentModel;
import com.sda.springproject.repository.StudentRepository;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Data
public class StudentService {

    //used for in memoery database
//    private List<StudentModel> students;

    private final StudentRepository repository;

    public Optional<StudentEntity> findById(Long id) {
        return repository.findById(id);
    }

    public Optional<StudentEntity> findByEmail(String email) {
        return repository.findByEmail(email);
    }

    public StudentEntity save(StudentEntity entity) {
        return repository.save(entity);
    }

    public StudentEntity updateStudent(StudentModel model) {
        Optional<StudentEntity> student = findById(model.getId());

        student.ifPresent(s -> {
            s.setFirstName(model.getFirstName());
            s.setLastName(model.getLastName());
            s.setBirthday(model.getBirthday());
        });

        StudentEntity updated = student.orElseThrow(() -> new NotFoundException("Cannot find student!"));
        return repository.save(updated);
    }

    public void delete(Long id) {
        repository.delete(findById(id).orElseThrow(() -> new NotFoundException("Cannot find student!")));
    }

    public List<StudentEntity> getAll() {
        return repository.findAll();
    }

    public List<StudentEntity> findAll(List<Long> ids) {
        List<StudentEntity> list = new ArrayList<>();

        for (Long l : ids) {
            list.add(repository.findById(l).orElse(null));
        }

        return list;
    }

    public List<StudentEntity> getAllByFirstName(String firstName) {
        return repository.findAllByFirstName(firstName);
    }

    public List<StudentEntity> getAllByBirthdayBetween(LocalDate from, LocalDate to) {
        return repository.getByBirthdayBetween(from, to);
    }

    // this is also used for in memory database
//    public StudentService() {
//        StudentModel s1 = new StudentModel(1, "Filan1", "Fistek1", LocalDate.of(1999, 10, 10));
//        StudentModel s2 = new StudentModel(2, "Filan2", "Fistek2", LocalDate.of(2002, 10, 10));
//        StudentModel s3 = new StudentModel(3, "Filan3", "Fistek3", LocalDate.of(2003, 10, 10));
//        this.students = new ArrayList<>(List.of(s1, s2, s3));
//    }
//
//    public Optional<StudentModel> findById(int id) {
//        return students.stream().filter(s -> s.getId() == id).findFirst();
//    }
//
//    public List<StudentModel> saveStudent(StudentModel student) {
//        if (students.stream().anyMatch(s -> s.getId() == student.getId())) {
//            throw new RuntimeException("Student with id: " + student.getId() + " already exists!");
//        }
//
//        students.add(student);
//
//        return students;
//    }
//
//    public StudentModel updateStudent(StudentModel model) {
//        if (students.stream().noneMatch(s -> s.getId() == model.getId())){
//            throw new NotFoundException("Student does not exist!");
//        }
//
//        StudentModel existing = findById(model.getId()).orElse(null);
//
//        existing.setId(model.getId());
//        existing.setFirstName(model.getFirstName());
//        existing.setLastName(model.getLastName());
//        existing.setBirthday(model.getBirthday());
//
//        return existing;
//    }
//
//    public void delete(int id) {
//        Optional<StudentModel> student = findById(id);
//
//        student.ifPresent(students::remove);
//    }
}
