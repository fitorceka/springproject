//package com.sda.springproject.service;
//
//import com.sda.springproject.config.AuthRequest;
//import com.sda.springproject.config.CustomUser;
//import lombok.AllArgsConstructor;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.stereotype.Service;
//
//@Service
//@AllArgsConstructor
//public class AuthService {
//
//    private AuthenticationManager authenticationManager;
//    private CustomUser userDetailsService;
//
//    public void authenticate(AuthRequest request) {
//        UserDetails user = userDetailsService.loadUserByUsername(request.getUsername());
//        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
//    }
//}
