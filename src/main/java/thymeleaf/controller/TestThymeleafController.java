package thymeleaf.controller;

import com.sda.springproject.mapper.StudentMapper;
import com.sda.springproject.model.StudentModel;
import com.sda.springproject.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class TestThymeleafController {

    private final StudentService studentService;
    private final StudentMapper mapper;

    @Autowired
    public TestThymeleafController(StudentService studentService, StudentMapper mapper) {
        this.studentService = studentService;
        this.mapper = mapper;
    }

    @GetMapping("/world")
    public String getHelloWorld(Model model) {
        String hello ="Hello World whtac upp";

        model.addAttribute("hel", hello);
        return "redirect:/display";
    }

    @GetMapping("/display")
    public String get() {
        return "display";
    }

    @GetMapping("/students")
    @PreAuthorize("hasAnyRole('ADMIN', 'STUDENT')")
    public String getAllStudents(Model model) {
        List<StudentModel> students = studentService.getAll()
                .stream().map(mapper::toModel).toList();

        model.addAttribute("students", students);

        return "students";
    }

    @GetMapping("/create")
    public String createStudentForm(Model model) {
        StudentModel studentModel = new StudentModel();
        model.addAttribute("studentModel", studentModel);
        return "create_student_form";
    }

    @PostMapping("/saveStudent")
    @Secured(value = "ROLE_ADMIN")
    public String saveStudent(@ModelAttribute("studentModel") StudentModel studentModel) {

        studentService.save(mapper.toEntity(studentModel));
        return "redirect:/students";
    }

    @DeleteMapping("/delete/{studentId}")
    @PreAuthorize("hasRole('ADMIN')")
    public String deleteStudent(@PathVariable("studentId") String id) {
        studentService.delete(Long.valueOf(id));
        return "redirect:/students";
    }

    @GetMapping("/updateForm")
    @PreAuthorize("hasRole('ADMIN')")
    public String updateForm(@RequestParam("studentId") Long id, Model model) {
        StudentModel s = studentService.findById(id).map(mapper::toModel).orElse(null);

        model.addAttribute("studentModel", s);

        return "update_student";
    }

    @PostMapping("/updateStudent")
    public String updateStudent(@ModelAttribute("studentModel") StudentModel model) {
        studentService.updateStudent(model);

        return "redirect:/students";
    }
}
